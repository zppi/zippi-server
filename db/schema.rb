# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_05_213821) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "auth_tokens", force: :cascade do |t|
    t.integer "user_id"
    t.string "session_token", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "verification_token"
    t.string "email", null: false
    t.index ["session_token"], name: "index_auth_tokens_on_session_token", unique: true
    t.index ["user_id"], name: "index_auth_tokens_on_user_id"
    t.index ["verification_token"], name: "index_auth_tokens_on_verification_token", unique: true
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uuid"], name: "index_groups_on_uuid"
  end

  create_table "groups_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "group_id", null: false
    t.index ["group_id", "user_id"], name: "index_groups_users_on_group_id_and_user_id", unique: true
    t.index ["group_id"], name: "index_groups_users_on_group_id"
    t.index ["user_id"], name: "index_groups_users_on_user_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "name", null: false
    t.integer "group_id", null: false
    t.text "img_path"
    t.text "qr_code", null: false
    t.integer "max_reservation_length"
    t.text "description"
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "custom_rules", default: [], array: true
    t.text "time_zone", default: "America/Los_Angeles", null: false
    t.integer "open_time"
    t.integer "close_time"
    t.index ["group_id"], name: "index_items_on_group_id"
    t.index ["uuid"], name: "index_items_on_uuid"
  end

  create_table "reservations", force: :cascade do |t|
    t.integer "item_id", null: false
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
    t.integer "user_id", null: false
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_reservations_on_item_id"
    t.index ["start_time"], name: "index_reservations_on_start_time"
    t.index ["user_id"], name: "index_reservations_on_user_id"
    t.index ["uuid"], name: "index_reservations_on_uuid"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", null: false
    t.uuid "uuid", default: -> { "uuid_generate_v4()" }, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["uuid"], name: "index_users_on_uuid"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "auth_tokens", "users", on_delete: :cascade
  add_foreign_key "groups_users", "groups", on_delete: :cascade
  add_foreign_key "groups_users", "users", on_delete: :cascade
  add_foreign_key "items", "groups", on_delete: :cascade
  add_foreign_key "reservations", "items", on_delete: :cascade
  add_foreign_key "reservations", "users", on_delete: :cascade
end
