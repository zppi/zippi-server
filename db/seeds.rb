def add_image(item, url)
  return if item.image.attached?

  item.image.attach(
    io: URI.open(url),
    filename: 'selfie.jpg',
    content_type: 'image/jpg'
  )
end

if Rails.env.development?
  # Users
  dev = User.find_or_create_by!(email: 'kylecorbitt@gmail.com')

  # Groups
  centennial = Group.find_or_create_by!(name: 'Centennial Apartments')
  byu = Group.find_or_create_by!(name: 'BYU')
  apt_667 = Group.find_or_create_by!(name: 'Apartment 667')

  GroupsUser.find_or_create_by!(group: centennial, user: dev)
  GroupsUser.find_or_create_by!(group: byu, user: dev)
  GroupsUser.find_or_create_by!(group: apt_667, user: dev)

  # Items
  bbq = Item.create_with(
    description: "Centennial barbequeue is open to Centennial residents and their guests. Please clean the grill and ensure the gas is fully off when you're finished. Contact the office if it isn't working.",
    custom_rules: [
      "Centennial Apartments residents only",
      "Max reservation length: 2 hours",
      "Clean BBQ after use"
    ],
    group: centennial
  ).find_or_create_by!(
    name: 'Centennial Barbecue',
  )
  add_image(bbq, 'https://c.pxhere.com/photos/ca/4b/grilling_hotdogs_hamburger_barbecue_grill_bbq_cooking_barbecue_grill-1054844.jpg!d')
  bbq_reservation = Reservation.find_or_initialize_by(user: dev, item: bbq)
  bbq_reservation.update!(
    start_time: Time.now.beginning_of_week + 1.week + 4.days + 18.hours,
    end_time: Time.now.beginning_of_week + 1.week + 4.days + 20.hours
  )

  firepit = Item.create_with(
    description: "Centennial firepit is open to Centennial residents and their guests. Wood is not provided. Ensure that the fire is fully extinguished and remove any unburned or partially burned wood from the pit when you're done.",
    custom_rules: [
      'Centennial Apartments residents only',
      'Max reservation length: 2 hours',
      'Ensure extinguished and clean firepit after use'
    ],
    group: centennial
  ).find_or_create_by!(
    name: 'Centennial Firepit'
  )
  add_image(firepit, 'https://cdn.shopify.com/s/files/1/0066/5095/3780/products/StahlX_1_1000x.jpg?v=1552098237')

  tv = Item.create_with(
    group: apt_667
  ).find_or_create_by!(
    name: 'Apt 667 TV'
  )

  centrifuge = Item.create_with(
    group: byu,
    description: 'BYU Life Sciences plasma centrifuge. Contact abrown@bio.byu.edu if machine is broken or if any spills occur.',
    custom_rules: [
      'Must be authorized by lab technician to use',
      'Leave machine clean'
    ]
  ).find_or_create_by!(
    name: 'BYU Biology Centrifuge'
  )
  add_image(centrifuge, 'https://i.pinimg.com/originals/0c/9e/05/0c9e05b3b57797183a388372e1bb4c97.jpg')

  threedee_printer = Item.create_with(
    group: byu,
    description: "Ideabots 3D Printer. Available in room 301 of the library. Ask the projects lab staff for assistance if you haven't used a 3D printer before. Also let the projects lab staff know if the machine is broken or low on filament.",
    custom_rules: [
      'Never leave unattended',
      'Stop machine if a build starts breaking',
      'Must finish print before library closes at 3pm',
      "Don't start a print if you won't have time to finish before your reservation ends"
    ]
  ).find_or_create_by!(name: 'HBLL 3D Printer')
  add_image(threedee_printer, 'http://www.3dengr.com/wp-content/uploads/2016/04/Public-Library-3d-printer-program.jpg')

  laser_cutter = Item.find_or_initialize_by(name: 'Ideas Lab Laser Cutter')
  laser_cutter.update!(
    group: byu,
    description: "The ideas lab laser cutter is in the Ideas Lab and is available to students for course work or senior projects. Contact abrewer@me.byu.edu for training.",
    open_time: 6.hours,
    close_time: 23.hours,
    time_zone: 'America/New_York',
    custom_rules: [
      'Never leave unattended',
      'Let ideas lab staff know if machine is broken',
      'Complete training before using machine'
    ]
  )
  add_image(laser_cutter, 'https://hackerlabrocklin.spaces.nexudus.com/en/events/getlargeimage?id=470877512&w=527.jpg')
  Reservation.
    find_or_initialize_by(user: dev, item: laser_cutter).
    update!(
      start_time: Time.now.in_time_zone('America/New_York').beginning_of_day + 1.day + 10.hours,
      end_time: Time.now.in_time_zone('America/New_York').beginning_of_day + 1.day + 11.hours
    )

  # A weird timezone for timezone bug desting
  kiwi = Item.find_or_initialize_by(name: 'Kiwi')
  kiwi.update!(
    group: byu,
    description: 'Literally just a kiwi',
    time_zone: 'Pacific/Auckland',
    open_time: 8.hours,
    close_time: 21.hours
  )
  add_image(kiwi, 'http://www.residencedomaso.com/en/wp-content/uploads/2018/04/kiwi-benessere-nutrione.jpg')
  Reservation.where(item: kiwi).destroy_all
  kiwi.update! reservations: [
    Reservation.new(
      user: dev,
      start_time: Time.now.in_time_zone(kiwi.time_zone).beginning_of_day + 1.day + 10.hours,
      end_time: Time.now.in_time_zone(kiwi.time_zone).beginning_of_day + 1.day + 11.hours
    ),
    Reservation.new(
      user: dev,
      start_time: Time.now.in_time_zone(kiwi.time_zone).beginning_of_day + 1.day + 13.hours,
      end_time: Time.now.in_time_zone(kiwi.time_zone).beginning_of_day + 1.day + 14.hours
    )
  ]

  # Tokens
  token = AuthToken.find_or_initialize_by(session_token: 'amanaplanacanal')
  token.update!(user: dev, email: dev.email)
end

puts "Seeding finished"
