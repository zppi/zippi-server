class UpdateAuthTokens < ActiveRecord::Migration[6.0]
  def change
    rename_column :auth_tokens, :token, :session_token
    add_column :auth_tokens, :verification_token, :string
    add_column :auth_tokens, :verfied, :boolean
  end
end
