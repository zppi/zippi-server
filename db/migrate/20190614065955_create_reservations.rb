class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.integer :item_id
      t.timestamp :start_time
      t.timestamp :end_time
      t.integer :user_id
      t.uuid :uuid

      t.timestamps
    end
  end
end
