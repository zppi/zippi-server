class ChangeDefaultTimezone < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:items, :time_zone, "America/Los_Angeles")
  end
end
