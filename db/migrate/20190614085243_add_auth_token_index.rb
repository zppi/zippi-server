class AddAuthTokenIndex < ActiveRecord::Migration[6.0]
  def change
    add_index :auth_tokens, :token
  end
end
