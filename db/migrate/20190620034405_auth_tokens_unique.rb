class AuthTokensUnique < ActiveRecord::Migration[6.0]
  def change
    remove_index :auth_tokens, :session_token
    remove_index :auth_tokens, :verification_token

    add_index :auth_tokens, :session_token, unique: true
    add_index :auth_tokens, :verification_token, unique: true
  end
end
