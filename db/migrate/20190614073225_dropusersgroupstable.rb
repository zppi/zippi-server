class Dropusersgroupstable < ActiveRecord::Migration[6.0]
  def up
    drop_table :users_groups
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
