class AddDefaultUuidValues < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:groups, :uuid, 'uuid_generate_v4()')
    change_column_default(:items, :uuid, 'uuid_generate_v4()')
    change_column_default(:reservations, :uuid, 'uuid_generate_v4()')
    change_column_default(:users, :uuid, 'uuid_generate_v4()')
  end
end
