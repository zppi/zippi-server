class RemoveActivationFromUsers < ActiveRecord::Migration[6.0]
  def up
    remove_column :users, :activation_digest
    remove_column :users, :activated
    remove_column :users, :activated_at
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
