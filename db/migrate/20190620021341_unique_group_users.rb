class UniqueGroupUsers < ActiveRecord::Migration[6.0]
  def change
    change_column_null :groups_users, :user_id, false
    change_column_null :groups_users, :group_id, false
    add_index :groups_users, [:group_id, :user_id], unique: true
  end
end
