class AddItemTimeZone < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :time_zone, :text, default: "Pacific Time (US & Canada)"
  end
end
