class MakeTimeZoneRequired < ActiveRecord::Migration[6.0]
  def change
    change_column_null :items, :time_zone, false
  end
end
