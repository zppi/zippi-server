class CorrectAuthTokenVerified < ActiveRecord::Migration[6.0]
  def change
    rename_column :auth_tokens, :verfied, :verified
  end
end
