class CreateGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups do |t|
      t.string :name
      t.uuid :uuid

      t.timestamps
    end
  end
end
