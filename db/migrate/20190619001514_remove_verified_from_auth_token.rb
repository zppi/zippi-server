class RemoveVerifiedFromAuthToken < ActiveRecord::Migration[6.0]
  def change
    remove_column :auth_tokens, :verified
  end
end
