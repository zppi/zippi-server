class ChangeItemTimeType < ActiveRecord::Migration[6.0]
  def change
    remove_column :items, :open_time
    remove_column :items, :close_time
    add_column :items, :open_time, :integer
    add_column :items, :close_time, :integer
  end
end
