class AddRulesToItems < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :custom_rules, :text, array: true, default: []
  end
end
