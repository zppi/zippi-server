class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :name
      t.integer :block_length
      t.integer :group_id
      t.text :img_path
      t.text :qr_code
      t.time :open_time
      t.time :close_time
      t.integer :max_reservation_length
      t.text :description
      t.uuid :uuid

      t.timestamps
    end
  end
end
