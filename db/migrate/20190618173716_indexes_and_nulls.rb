class IndexesAndNulls < ActiveRecord::Migration[6.0]
  def change
    # Auth tokens
    change_column_null :auth_tokens, :session_token, false
    change_column_null :auth_tokens, :email, false
    change_column_null :auth_tokens, :verified, false
    change_column_default :auth_tokens, :verified, from: nil, to: false

    add_index :auth_tokens, :verification_token
    add_index :auth_tokens, :user_id

    add_foreign_key :auth_tokens, :users, on_delete: :cascade

    # Groups
    change_column_null :groups, :uuid, false
    
    add_index :groups, :uuid

    add_foreign_key :groups_users, :users, on_delete: :cascade
    add_foreign_key :groups_users, :groups, on_delete: :cascade

    # Items
    change_column_null :items, :name, false
    change_column_null :items, :group_id, false
    change_column_null :items, :qr_code, false
    change_column_null :items, :uuid, false

    add_index :items, :group_id
    add_index :items, :uuid

    add_foreign_key :items, :groups, on_delete: :cascade

    # Reservations
    change_column_null :reservations, :item_id, false
    change_column_null :reservations, :start_time, false
    change_column_null :reservations, :end_time, false
    change_column_null :reservations, :user_id, false
    change_column_null :reservations, :uuid, false

    add_index :reservations, :uuid
    add_index :reservations, :item_id
    add_index :reservations, :user_id
    add_index :reservations, :start_time

    add_foreign_key :reservations, :items, on_delete: :cascade
    add_foreign_key :reservations, :users, on_delete: :cascade

    # Users
    change_column_null :users, :name, false
    change_column_null :users, :email, false
    change_column_null :users, :uuid, false

    add_index :users, :uuid
  end
end
