class ItemController < ApplicationController
  protect_from_forgery unless: -> {true}

  def image
    token = request.headers['Authorization']&.split(' ')&.last
    return nil unless token
    id = request.headers['X-Item-Id']&.split(' ')&.last
    return nil unless id

    @current_user ||= AuthToken.find_by(session_token: token)&.user

    @item = Item.find_by(uuid: id)

    @item.image.attach(params[:picture])
  end
end
