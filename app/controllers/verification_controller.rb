class VerificationController < ApplicationController
  protect_from_forgery unless: -> {true}

  def app_redirect
    @token_valid = params[:verification_token].present? && 
      AuthToken.where(verification_token: params[:verification_token]).any?

    @continue_url = if @token_valid
      "#{params[:app_callback]}?verificationToken=#{URI.encode(params[:verification_token])}"
    else
      params[:app_callback]
    end
  end
end
