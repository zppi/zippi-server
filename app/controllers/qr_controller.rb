class QrController < ApplicationController
  def sticker
    template = Slim::Template.new(
      Rails.root.join("app/views/qr/#{params[:template] || 'fun'}.slim")
    )
    template_html = template.render(self)
    kit = IMGKit.new(template_html, { width: (300*2.24).to_i, height:(300*3.39).to_i })
    image = kit.to_png
    send_data image, type: 'image/png', disposition: 'inline'
  end

  private

  def qr_logo_html(item = "")
    code = Item.valid_qr_code
    qrcode = RQRCode::QRCode.new("https://zippi.app/i/#{code}", :size => 7, :level => :h);
    qrimage = qrcode.as_png(size: 1000)
    return <<-HTML
      <img src="https://zippi-assets.s3-us-west-2.amazonaws.com/logo.png" class="logo"/>
      <img src="#{qrimage.to_data_url}" class="qr-code" />
      <div class="item-code"><span style="color: #666">#{item}</span>#{code}</div>
    HTML
  end
end
