# == Schema Information
#
# Table name: reservations
#
#  id         :bigint           not null, primary key
#  end_time   :datetime         not null
#  start_time :datetime         not null
#  uuid       :uuid             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  item_id    :integer          not null
#  user_id    :integer          not null
#
# Indexes
#
#  index_reservations_on_item_id     (item_id)
#  index_reservations_on_start_time  (start_time)
#  index_reservations_on_user_id     (user_id)
#  index_reservations_on_uuid        (uuid)
#
# Foreign Keys
#
#  fk_rails_...  (item_id => items.id) ON DELETE => cascade
#  fk_rails_...  (user_id => users.id) ON DELETE => cascade
#

class Reservation < ApplicationRecord
  validates :start_time, presence: true
  validates :end_time, presence: true
  validate :start_time_valid
  validate :end_time_valid

  belongs_to :item
  belongs_to :user

  private

  def start_time_valid
    start_time_tz = start_time&.in_time_zone(item.time_zone)
    if item&.open_time && item.open_time > start_time_tz.seconds_since_midnight
      formatted_open_time = (start_time_tz.beginning_of_day + item.open_time).strftime("%-l:%M%P %Z")
      errors.add(:start_time, "Can't be reserved before #{formatted_open_time}")
    end
  end

  def end_time_valid
    end_time_tz = end_time&.in_time_zone(item.time_zone)
    if item&.close_time && item.close_time < end_time_tz.seconds_since_midnight
      formatted_close_time = (end_time_tz.beginning_of_day + item.close_time).strftime("%-l:%M%P %Z")
      errors.add(:end_time, "Can't be reserved after #{formatted_close_time}")
    end
  end
end
