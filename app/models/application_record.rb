class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def all_errors
    errors.messages.map do |f, errors| "#{f.to_s.titleize}: #{errors.join(', ')}." end.join("\n")
  end
end
