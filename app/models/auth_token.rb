# == Schema Information
#
# Table name: auth_tokens
#
#  id                 :bigint           not null, primary key
#  email              :string           not null
#  session_token      :string           not null
#  verification_token :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :integer
#
# Indexes
#
#  index_auth_tokens_on_session_token       (session_token) UNIQUE
#  index_auth_tokens_on_user_id             (user_id)
#  index_auth_tokens_on_verification_token  (verification_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id) ON DELETE => cascade
#

class AuthToken < ApplicationRecord
  validates :session_token, presence: true
  validates :email, presence: true

  belongs_to :user, required: false
end
