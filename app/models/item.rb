# == Schema Information
#
# Table name: items
#
#  id                     :bigint           not null, primary key
#  close_time             :integer
#  custom_rules           :text             default([]), is an Array
#  description            :text
#  img_path               :text
#  max_reservation_length :integer
#  name                   :string           not null
#  open_time              :integer
#  qr_code                :text             not null
#  time_zone              :text             default("America/Los_Angeles"), not null
#  uuid                   :uuid             not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  group_id               :integer          not null
#
# Indexes
#
#  index_items_on_group_id  (group_id)
#  index_items_on_uuid      (uuid)
#
# Foreign Keys
#
#  fk_rails_...  (group_id => groups.id) ON DELETE => cascade
#

class Item < ApplicationRecord
  before_validation :set_qr_code, on: :create

  validates :name, presence: true, length: { maximum: 50 }
  validates :group_id, presence: true, numericality: { only_integer: true }
  validates :qr_code, presence: true

  belongs_to :group
  has_many :reservations
  has_one_attached :image

  def find_by_qr_code(code)
    # If we get a QR code of the form "zippi.app/i/xxx-xxx-xxx" this looks for an
    # item in the database with a code of the form "xxxxxxxxx"
    return nil unless code
    find_by(qr_code: code&.split('/')&.last&.gsub('-', ''))
  end

  def rules
    # TODO we should autogen some rules about max reservation length, etc.
    custom_rules
  end

  UNAMBIGUOUS_CHARS = [*'A'..'Z', *'0'..'9'] - %w{B 8 1 l I 0 O o}
  def self.valid_qr_code
    (0...7).map { UNAMBIGUOUS_CHARS[SecureRandom.rand(0...UNAMBIGUOUS_CHARS.length)] }.join
  end

  private

  def set_qr_code
    self.qr_code = self.class.valid_qr_code
  end
end
