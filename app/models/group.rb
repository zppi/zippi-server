# == Schema Information
#
# Table name: groups
#
#  id         :bigint           not null, primary key
#  name       :string
#  uuid       :uuid             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_groups_on_uuid  (uuid)
#

class Group < ApplicationRecord
  validates :name,  presence: true, length: { maximum: 50 }
  
  has_and_belongs_to_many :users
  has_many :items
end
