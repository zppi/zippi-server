class Mutations::RequestLoginEmail < Mutations::BaseMutation
  include Email

  argument :email, String, required: true
  argument :callback_url, String, required: true

  field :email, String, null: false

  def resolve(email: nil, callback_url: nil)
    if !is_a_valid_email?(email)
      return GraphQL::ExecutionError.new("That email doesn't seem valid, please try another")
    end

    unless %w(exp:// zippi://).any? { |prefix| callback_url.starts_with?(prefix) }
      # TODO server-side error reporting
      return GraphQL::ExecutionError.new("Our app seems to have encountered an error. Please contact help@zippi.app")
    end

    auth_token = AuthToken.create!(
      user: User.find_by(email: email.downcase), # May be nil
      session_token: SecureRandom.urlsafe_base64(30),
      verification_token: SecureRandom.urlsafe_base64(30),
      email: email.downcase
    )

    AuthMailer.magic_link(
      context[:request].host,
      auth_token,
      callback_url
    ).deliver

    auth_token
  end

end
