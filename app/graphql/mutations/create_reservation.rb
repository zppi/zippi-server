module Mutations
  class CreateReservation < BaseMutation
    include Reserve
    include Authorize
    # arguments passed to the `resolved` method
    argument :item_id, String, required: true
    argument :start_time, Types::DateTimeType, required: true
    argument :end_time, Types::DateTimeType, required: true

    # return type from the mutation
    type Types::ReservationType

    RANDOM_UUID = "421d53f3-f7e7-42e9-832a-e8d95097d0f0"

    def resolve(item_id:, start_time:, end_time:)
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to create a reservation")
      end

      @item = Item.find_by(uuid: item_id)
      if !@item
        return GraphQL::ExecutionError.new("Invalid input: the item id you provided does not match an item in the database")
      end

      if !inGroup?(@current_user, @item.group)
        return GraphQL::ExecutionError.new("Invalid input: you cannot create a reservation on an item in a group that you are not a member of")
      end

      if !validLength?(@item, start_time, end_time)
        return GraphQL::ExecutionError.new("Invalid input: you provided an invalid time length for a reservation on this item")
      end
      if !validSlot?(RANDOM_UUID, @item, start_time, end_time)
        return GraphQL::ExecutionError.new("Invalid input: the time slot requested is not available on this item")
      end

      @reservation = Reservation.create(
        item_id: @item.id,
        start_time: start_time,
        end_time: end_time,
        user_id: @current_user.id
      )

      if !@reservation.valid?
        return GraphQL::ExecutionError.new(@reservation.all_errors)
      end

      return @reservation.reload
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
