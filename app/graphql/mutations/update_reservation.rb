module Mutations
  class UpdateReservation < BaseMutation
    include Reserve
    include Authorize
    # arguments passed to the `resolved` method
    argument :id, String, required: true
    argument :start_time, String, required: true
    argument :end_time, String, required: true

    # return type from the mutation
    type Types::ReservationType

    def resolve(id:, start_time:, end_time:)
      start_time = Time.zone.parse(start_time)
      end_time = Time.zone.parse(end_time)
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to update this reservation")
      end

      @reservation = Reservation.find_by(uuid: id)
      if !@reservation
        return GraphQL::ExecutionError.new("Invalid input: the reservation id you provided does not match a reservation in the database")
      end

      if !owns_reservation?(@current_user, @reservation)
        return GraphQL::ExecutionError.new("Invalid input: you cannot update a reservation that you do not own")
      end

      @item = @reservation.item

      if !validLength?(@item, start_time, end_time)
        return GraphQL::ExecutionError.new("Invalid input: you provided an invalid time length for a reservation on this item")
      end
      if !validSlot?(id, @item, start_time, end_time)
        return GraphQL::ExecutionError.new("Invalid input: the time slot requested is not available on this item")
      end

      @reservation.start_time = start_time
      @reservation.end_time = end_time

      if !@reservation.save
        return GraphQL::ExecutionError.new(@reservation.all_errors)
      end

      return @reservation.reload
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
