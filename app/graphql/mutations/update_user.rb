module Mutations
  class UpdateUser < BaseMutation
    include Authenticate
    # arguments passed to the `resolved` method
    argument :name, String, required: false
    argument :email, String, required: false

    # return type from the mutation
    type Types::UserType

    def resolve(name: nil, email: nil)
      @current_user = context[:current_user]
      if name
        @current_user.name = name
      end
      if email
        @current_user.email = email
      end

      if @current_user.valid?
        @current_user.save
      end
      return @current_user
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end

  end
end
