module Mutations
  class DeleteReservation < BaseMutation
    include Authorize
    # arguments passed to the `resolved` method
    argument :id, String, required: true

    # return type from the mutation
    field :id, String, null: false
    field :message, String, null: false
    field :success, Boolean, null: false

    def resolve(id:)
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to update this reservation")
      end

      @reservation = Reservation.find_by(uuid: id)
      if !@reservation
        return GraphQL::ExecutionError.new("Invalid input: the reservation id you provided does not match a reservation in the database")
      end

      unless owns_reservation?(@current_user, @reservation)
        return GraphQL::ExecutionError.new("You cannot delete a reservation that you do not own")
      end

      @reservation.destroy
      return  {
                message: "Delete reservation with id #{id}",
                success: true,
                id: id
              }
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
