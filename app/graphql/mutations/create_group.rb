module Mutations
  class CreateGroup < BaseMutation
    # arguments passed to the `resolved` method
    argument :name, String, required: true

    # return type from the mutation
    type Types::GroupType

    def resolve(name:)
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to create a group")
      end
      if Group.find_by(name: name)
        return GraphQL::ExecutionError.new("Invalid input: group name #{name} is already taken")
      end

      @group = Group.create(name: name)
      @group.users.push(@current_user)
      return @group.reload
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
