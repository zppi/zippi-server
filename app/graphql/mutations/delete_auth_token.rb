module Mutations
  class DeleteAuthToken < BaseMutation
    include Authenticate
    # arguments passed to the `resolved` method
    argument :session_token, String, required: true

    # return type from the mutation
    field :message, String, null: false
    field :success, Boolean, null: false

    def resolve(session_token: nil)
      if deleteAuth(session_token)
        return  {
                  message: "Delete authToken",
                  success: true
                }
      end
      return GraphQL::ExecutionError.new("Invalid input: invalid session_token")
    end
  end
end
