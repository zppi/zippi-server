module Mutations
  class UpdateItem < BaseMutation
    # arguments passed to the `resolved` method
    argument :id, String, required: true
    argument :name, String, required: false
    argument :img_path, String, required: false
    argument :open_time, Integer, required: false
    argument :close_time, Integer, required: false
    argument :max_reservation_length, Integer, required: false
    argument :description, String, required: false
    argument :time_zone, String, required: false
    argument :rules, [String], required: false

    # return type from the mutation
    type Types::ItemType

    def resolve(id:, name: nil, img_path: nil, open_time: nil, close_time: nil, max_reservation_length: nil, description: nil, time_zone: nil, rules: nil)
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to update an item")
      end

      @item = Item.find_by(uuid: id)

      if !@item
        return GraphQL::ExecutionError.new("Invalid input: the id you provided does not match an item in the database")
      end

      if Item.find_by_group_id_and_name(@item.group.id, name) && Item.find_by_group_id_and_name(@item.group.id, name).uuid != id
        return GraphQL::ExecutionError.new("Invalid input: an item in this group already has the name you chose. Please choose another")
      end
      if name
        @item.name = name
      end
      if img_path
        @item.img_path = img_path
      end

      if !time_zone.present?
        time_zone = @item.time_zone
      end

      if open_time
        @item.open_time = open_time
      end
      if close_time
        @item.close_time = close_time
      end
      if max_reservation_length.present?
        @item.max_reservation_length = max_reservation_length
      end
      if description
        @item.description = description
      end

      if rules.present? || rules == []
        @item.custom_rules = rules
      end

      if !@item.valid?
        return GraphQL::ExecutionError.new("Invalid input: one or more values passed in to update the item was not acceptable")
      end

      @item.save
      return @item.reload
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
