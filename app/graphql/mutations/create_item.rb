module Mutations
  class CreateItem < BaseMutation
    include Authorize
    # arguments passed to the `resolved` method
    argument :group_name, String, required: false
    argument :group_id, String, required: false
    argument :name, String, required: true
    argument :img_path, String, required: false
    argument :open_time, Integer, required: false
    argument :close_time, Integer, required: false
    argument :max_reservation_length, Integer, required: false
    argument :description, String, required: false
    argument :qr_code, String, required: false
    argument :time_zone, String, required: false
    argument :rules, [String], required: false

    # return type from the mutation
    type Types::ItemType

    def resolve(group_name: nil, group_id: nil, name:, img_path: nil, open_time: nil, close_time: nil, max_reservation_length: nil, description: nil, qr_code: nil, time_zone: "America/Los_Angeles", rules: [])
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to create an item")
      end

      @group = nil

      if group_id.present? # client is trying to find an existing group. This can only be done through group uuid
        @group = Group.find_by(uuid: group_id)
        if !@group
          return GraphQL::ExecutionError.new("Invalid input: group id #{group_id} matches no group")
        end
      elsif group_name.present? # client is trying to create a new group
        if Group.find_by(name: group_name)
          return GraphQL::ExecutionError.new("Invalid input: group name #{group_name} is already taken")
        end
        @group = Group.create(name: group_name)
        @group.users.push(@current_user)
      else # client failed to provide information to create or find a group
        return GraphQL::ExecutionError.new("Invalid input: please create or select a group to put this item in")
      end

      if !inGroup?(@current_user, @group)
        return GraphQL::ExecutionError.new("Invalid input: you cannot create an item in a group that you are not a member of")
      end

      if Item.find_by_group_id_and_name(@group.id, name)
        return GraphQL::ExecutionError.new("Invalid input: an item in this group already has the name you chose. Please choose another")
      end

      if open_time
        open_time = open_time
      end

      if close_time
        close_time = close_time
      end

      @item = Item.new(
        name: name,
        group_id: @group.id,
        img_path: img_path,
        open_time: open_time,
        close_time: close_time,
        max_reservation_length: max_reservation_length,
        description: description,
        qr_code: qr_code,
        time_zone: time_zone,
        custom_rules: rules
      )

      if !@item.valid?
        return GraphQL::ExecutionError.new("Invalid input: one or more values passed in to create the new item was not acceptable")
      end

      @item.save
      return @item.reload
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
