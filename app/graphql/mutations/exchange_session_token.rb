class Mutations::ExchangeSessionToken < Mutations::BaseMutation
  argument :verification_token, String, required: true

  field :sessionToken, String, null: false
  field :email, String, null: false

  def resolve(verification_token: nil)
    auth_token = AuthToken.
      where.not(verification_token: nil).
      find_by(verification_token: verification_token)

    return(GraphQL::ExecutionError.new("Verification token not found")) unless auth_token

    auth_token.update!(
      verification_token: nil,
      user: User.find_or_create_by!(email: auth_token.email)
    )

    auth_token
  rescue ActiveRecord::RecordInvalid => e
    GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
  end
end
