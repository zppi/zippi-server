module Mutations
  class UpdateGroup < BaseMutation
    include Authorize
    # arguments passed to the `resolved` method
    argument :id, String, required: true
    argument :name, String, required: false

    # return type from the mutation
    type Types::GroupType

    def resolve(id:, name: nil)
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to update a group")
      end

      @group = Group.find_by(uuid: id)

      if !@group
        return GraphQL::ExecutionError.new("Invalid input: the id you provided does not match a group in the database")
      end

      if !inGroup?(@current_user, @group)
        return GraphQL::ExecutionError.new("You must be a member of the group to update it")
      end

      if Group.find_by(name: name) && Group.find_by(name: name).uuid != id
        return GraphQL::ExecutionError.new("Invalid input: group name #{name} is already taken")
      end
      if name
        @group.name = name
      end

      if !@group.valid?
        return GraphQL::ExecutionError.new("Invalid input: one or more values passed in to update the group was not acceptable")
      end

      @group.save
      return @group.reload
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
