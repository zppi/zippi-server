module Mutations
  class JoinGroup < BaseMutation
    include Authorize
    # arguments passed to the `resolved` method
    argument :group_id, String, required: true

    # return type from the mutation
    type Types::UserDataType

    def resolve(group_id:)
      @current_user = context[:current_user]
      if !@current_user
        return GraphQL::ExecutionError.new("You must be logged in to add this group")
      end

      @group = Group.find_by(uuid: group_id)
      if !@group
        return GraphQL::ExecutionError.new("Invalid input: the group id you provided does not match a group in the database")
      end

      if inGroup?(@current_user, @group)
        return GraphQL::ExecutionError.new("You are already a member of this group")
      end

      @current_user.groups.push(@group)
      return @current_user.reload
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end

  end
end
