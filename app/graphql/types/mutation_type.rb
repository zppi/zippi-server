module Types
  class MutationType < Types::BaseObject
    field :request_login_email, mutation: Mutations::RequestLoginEmail
    field :exchange_session_token, mutation: Mutations::ExchangeSessionToken
    field :delete_auth_token, mutation: Mutations::DeleteAuthToken
    field :update_user, mutation: Mutations::UpdateUser
    field :create_group, mutation: Mutations::CreateGroup
    field :update_group, mutation: Mutations::UpdateGroup
    field :join_group, mutation: Mutations::JoinGroup
    field :create_item, mutation: Mutations::CreateItem
    field :update_item, mutation: Mutations::UpdateItem
    field :create_reservation, mutation: Mutations::CreateReservation
    field :update_reservation, mutation: Mutations::UpdateReservation
    field :delete_reservation, mutation: Mutations::DeleteReservation
  end
end
