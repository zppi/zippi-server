module Types
  class QueryType < Types::BaseObject
    field :user_data, Types::UserDataType, null: true do
      argument :id, String, required: true
    end
    field :group_data, Types::GroupType, null: true do
      argument :id, String, required: true
    end
    field :item_data, Types::ItemType, null: true do
      argument :id, String, required: true
    end
    field :reservation_data, Types::ReservationType, null: true do
      argument :id, String, required: true
    end
    field :my_groups, [Types::GroupType], null: true
    field :my_reservations, [Types::ReservationType], null: true

    def initial_data
      context[:current_user]
    end

    def user_data(id:)
      User.find_by(uuid: id)
    end

    def group_data(id:)
      Group.find_by(uuid: id)
    end

    def item_data(id:)
      Item.find_by(uuid: id)
    end

    def reservation_data(id:)
      Reservation.find_by(uuid: id)
    end

    def my_groups
      context[:current_user].groups
    end

    def my_reservations
      context[:current_user].reservations
    end
  end
end
