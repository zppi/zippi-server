module Types
  class BaseObject < GraphQL::Schema::Object
    def id
      object.uuid
    end

    def uuid
      raise "The client should always refer to `uuid` as `id`. Try `field :id` instead of `field :uuid`"
    end

    def current_user
      context[:current_user]
    end
  end
end
