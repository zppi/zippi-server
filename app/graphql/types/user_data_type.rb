module Types
  class UserDataType < BaseObject
    field :name, String, null: true
    field :email, String, null: false
    field :groups, [GroupType], null: false
    field :reservations, [ReservationType], null: false
    field :id, String, null: false
  end
end
