module Types
  class ReservationType < BaseObject
    include Authorize

    field :id, String, null: false
    field :item, ItemType, null: false
    field :user, UserDataType, null: false
    field :start_time, DateTimeType, null: false
    field :end_time, DateTimeType, null: false
    field :can_delete, Boolean, null: false

    def can_delete
      owns_reservation?(current_user, object)
    end  
  end
end
