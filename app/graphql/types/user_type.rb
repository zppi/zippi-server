module Types
  class UserType < BaseObject
    field :name, String, null: true
    field :email, String, null: false
    field :id, String, null: false
  end
end
