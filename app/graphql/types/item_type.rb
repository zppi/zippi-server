module Types
  class ItemType < BaseObject
    field :name, String, null: false
    field :img_path, String, null: true
    field :qr_code, String, null: false
    field :open_time, Integer, null: true
    field :close_time, Integer, null: true
    field :max_reservation_length, Integer, null: true
    field :description, String, null: true
    field :id, String, null: false
    field :reservations, [ReservationType], null: false do
      argument :after, DateTimeType, required: false
      argument :before, DateTimeType, required: false
    end
    field :has_reserved, Boolean, null: false
    field :group, GroupType, null: false # yet to test this one
    field :rules, [String], null: false
    field :time_zone, String, null: false

    def has_reserved
      current_user.reservations.where(item: object).any?
    end

    def img_path
      if object.image&.attachment
        return object.image.service_url
      end
      nil
    end

    def reservations(after: Time.now, before: nil)
      reservations = object.reservations
      reservations = reservations.where('end_time >= ?', after) if after
      reservations = reservations.where('start_time <= ?', before) if before

      reservations
    end
  end
end
