module Types
  class GroupType < BaseObject
    field :name, String, null: false
    field :items, [ItemType], null: false
    field :users, [UserType], null: false
    field :id, String, null: false
  end
end
