class AuthMailer < ApplicationMailer
  default from: "Zippi App <noreply@zippi.app>"

  def magic_link(backend_host, auth_token, callback_url)
    @app_redirect = app_redirect_url(
      host: backend_host,
      app_callback: callback_url,
      verification_token: auth_token.verification_token
    )

    mail(
      to: auth_token.email,
      subject: 'Sign into your Zippi account!'
    )
  end

end
