module Authenticate
  def deleteAuth(session_token)
    @auth_token = AuthToken.find_by_session_token(session_token)
    if !!@auth_token
      @auth_token.destroy
      return true
    end
    return false
  end
end
