module Reserve
# .utc.seconds_since_midnight.to_i
  def validLength?(item, start_time, end_time)
    if start_time > end_time
      return false
    end
    if item.max_reservation_length != nil && item.max_reservation_length < ((end_time.to_i - start_time.to_i) / 1.minute).round
      return false
    end
    return true
  end

  def validSlot?(reservation_uuid, item, start_time, end_time)
    if item.reservations.where('end_time > ?', start_time).where('start_time < ?', end_time).where.not(uuid: reservation_uuid).any?
      return false
    end
    return true
  end

end
