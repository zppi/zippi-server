module Authorize

  def inGroup?(user, group)
    return user.groups.include?(group)
  end

  def owns_reservation?(user, reservation)
    # TODO allow admins to cancel reservations

    return reservation.user == user
  end

end
