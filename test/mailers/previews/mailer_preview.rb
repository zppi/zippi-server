class MailerPreview < ActionMailer::Preview
  def magic_link
    token = AuthToken.new(email: 'kylecorbitt@gmail.com', verification_token: 'fake')
    AuthMailer.magic_link('localhost:3000', token, 'exp://test/app')
  end
end

# token = AuthToken.new(email: 'kylecorbitt@gmail.com', verification_token: 'fake'); AuthMailer.magic_link('localhost:3000', token, 'exp://test/app').deliver
