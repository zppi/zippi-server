require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  # INITIAL_DATA_QUERY_STRING = "{
  #   initialData { name email uuid
  #     reservations { uuid
  #       item {
  #               name
  #             }
  #       user {
  #         name
  #       }
  #     }
  #     groups { name uuid
  #       items { name blockLength imgPath qrCode openTime closeTime maxReservationLength description uuid
  #         reservations {
  #           user {
  #             name
  #           }
  #         }
  #       }
  #     }
  #   }
  # }"
  #
  # test "send email to invalid email" do
  #   assert_nil AuthToken.find_by(email: 'aa')
  #   post "/graphql",
  #     params: {
  #     	"query": "mutation { createAuthToken(email: \"aa\") {email sessionToken }}"
  #     }
  #   assert_response :success
  #   object = JSON.parse(@response.body)
  #   assert_equal "Invalid input: invalid email", object["errors"][0]["message"]
  #   assert_nil AuthToken.find_by(email: 'aa')
  # end
  #
  # test "send email to valid email" do
  #   assert_nil AuthToken.find_by(email: 'a@b.com')
  #   post "/graphql",
  #     params: {
  #       "query": "mutation { createAuthToken(email: \"a@b.com\") {email sessionToken }}"
  #     }
  #   assert_response :success
  #   object = JSON.parse(@response.body)
  #   assert_equal "a@b.com", object["data"]["createAuthToken"]["email"]
  #   assert_equal !!object["data"]["createAuthToken"]["sessionToken"], true
  #   assert_equal !!AuthToken.find_by(email: 'a@b.com'), true
  # end
  #
  # test "verify invalid token" do
  #   assert_nil AuthToken.find_by(verification_token: 'invalid_verification_token')
  #   get "/verify",
  #     params: { verification_token: 'invalid_verification_token'}
  #   assert_response :success
  #   assert_nil AuthToken.find_by(verification_token: 'invalid_verification_token')
  # end
  #
  # test "verify valid token" do
  #   assert_equal !!AuthToken.find_by(verification_token: 'verification_token2'), true
  #   assert_nil User.find_by(email: 'valid2@email.com')
  #   get "/verify",
  #     params: { verification_token: 'verification_token2'}
  #   assert_response :success
  #   assert_nil AuthToken.find_by(verification_token: 'verification_token2')
  #   assert_equal !!User.find_by(email: 'valid2@email.com'), true
  # end
  #
  # test "verify already-verified valid token" do
  #   assert_equal !!AuthToken.find_by(verification_token: 'verification_token1'), true
  #   get "/graphql",
  #     params: {
  #       "query": "mutation { verifyAuthToken(verificationToken: \"verification_token1\") { message success }}"
  #     }
  #   assert_response :success
  #   assert_nil object
  #   assert_nil AuthToken.find_by(verification_token: 'verification_token1')
  # end
  #
  # test "request initial data with invalid token" do
  #   get "/graphql",
  #     params: {
  #       "query": INITIAL_DATA_QUERY_STRING
  #     },
  #     headers: { 'Authorization': 'bearer invalid_token' }
  #   assert_response :success
  #   object = JSON.parse(@response.body)
  #   assert_nil object["data"]["initialData"]
  # end
  #
  # test "request initial data with valid, unverified token" do
  #   get "/graphql",
  #     params: {
  #       "query": INITIAL_DATA_QUERY_STRING
  #     },
  #     headers: { 'Authorization': 'bearer unverified_session_token' }
  #   assert_response :success
  #   object = JSON.parse(@response.body)
  #   assert_nil object["data"]["initialData"]
  # end
  #
  # test "request initial data with no token" do
  #   get "/graphql",
  #     params: {
  #       "query": INITIAL_DATA_QUERY_STRING
  #     }
  #   assert_response :success
  #   object = JSON.parse(@response.body)
  #   assert_nil object["data"]["initialData"]
  # end
  #
  # test "request initial data with valid, verified token" do
  #   get "/graphql",
  #     params: {
  #       "query": INITIAL_DATA_QUERY_STRING
  #     },
  #     headers: { 'Authorization': 'bearer verified_session_token' }
  #   assert_response :success
  #   object = JSON.parse(@response.body)
  #   assert_equal object["data"]["initialData"]["name"], "Dave"
  #   assert_equal object["data"]["initialData"]["email"], "davidlcorbitt@gmail.com"
  #   assert_equal object["data"]["initialData"]["uuid"], "0e72b2d6-aae3-4aed-8562-3c57d34cce58"
  #   assert_equal object["data"]["initialData"]["reservations"][0]["uuid"], "b1bc064c-4f21-4dcf-a58c-19ad2cb772dc"
  #   assert_equal object["data"]["initialData"]["reservations"][0]["item"]["name"], "Piano"
  #   assert_equal object["data"]["initialData"]["reservations"][0]["user"]["name"], "Dave"
  #   assert_equal object["data"]["initialData"]["groups"][0]["name"], "First Group"
  #   assert_equal object["data"]["initialData"]["groups"][0]["uuid"], "2c236840-2294-4846-9d95-e55526a92aee"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["name"], "Piano"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["imgPath"], "some_img_path"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["qrCode"], "some_code"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["openTime"], "2000-01-01T08:00:00Z"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["closeTime"], "2000-01-01T21:00:00Z"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["maxReservationLength"], 60
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["description"], "This is a piano that anyone can use"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["uuid"], "7a5c4dad-5851-4df7-bcc5-a4198f1f3eda"
  #   assert_equal object["data"]["initialData"]["groups"][0]["items"][0]["reservations"][0]["user"]["name"], "Dave"
  #   assert_equal object["data"]["initialData"]["groups"][1]["name"], "Second Group"
  #   assert_equal object["data"]["initialData"]["groups"][1]["uuid"], "e222b912-3989-4e2e-9a92-44635ac198cf"
  #   assert_equal object["data"]["initialData"]["groups"][1]["items"], []
  # end

end
