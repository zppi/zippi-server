require 'test_helper'

class CreateUpdateDeleteReservationsTest < ActionDispatch::IntegrationTest

  test "create reservation not in group" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 23:59:57", "2019-06-13 23:59:58", "5c247e11-c0ba-41a1-a5dc-7ff25888d0f5")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["errors"][0]["message"], "Invalid input: you cannot create a reservation on an item in a group that you are not a member of"
  end

  test "join group create reservation" do
    post "/graphql",
      params: {
        "query": "mutation {
                    joinGroup(
                      groupId: \"2726f645-837f-45eb-bf80-1c64408f3fa6\"
                    ) {
                      groups {
                        id
                      }
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }

    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 23:59:57", "2019-06-13 23:59:58", "5c247e11-c0ba-41a1-a5dc-7ff25888d0f5")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "2019-06-14T06:59:57Z", object["data"]["createReservation"]["startTime"]
    assert_equal "2019-06-14T06:59:58Z", object["data"]["createReservation"]["endTime"]
  end

  test "create reservation" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:10:00", "2019-06-13 9:20:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "2019-06-13T13:10:00Z", object["data"]["createReservation"]["startTime"]
    assert_equal "2019-06-13T13:20:00Z", object["data"]["createReservation"]["endTime"]
  end

  test "create reservation too early" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 8:10:00", "2019-06-13 8:20:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_match /Can't be reserved before 9:00am/, object["errors"][0]["message"]
  end

  test "create reservation too late" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 20:10:00", "2019-06-13 20:20:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_match /Can't be reserved after 5:00pm/, object["errors"][0]["message"]
  end

  test "create two reservations" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:10:00", "2019-06-13 9:20:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
      post "/graphql",
        params: {
        	"query": createReservationQuery("2019-06-13 9:20:00", "2019-06-13 9:30:00")
        },
        headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "2019-06-13T13:20:00Z", object["data"]["createReservation"]["startTime"]
    assert_equal "2019-06-13T13:30:00Z", object["data"]["createReservation"]["endTime"]
  end

  test "create two reservations different days same times" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:10:00", "2019-06-13 9:20:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
      post "/graphql",
        params: {
        	"query": createReservationQuery("2019-06-12 9:05:00", "2019-06-12 9:30:00")
        },
        headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "2019-06-12T13:05:00Z", object["data"]["createReservation"]["startTime"]
    assert_equal "2019-06-12T13:30:00Z", object["data"]["createReservation"]["endTime"]
  end

  test "create reservation bad length" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:10:00", "2019-06-13 9:50:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["errors"][0]["message"], "Invalid input: you provided an invalid time length for a reservation on this item"
  end

  test "create and update to overlapping reservations" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:10:00", "2019-06-13 9:20:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }

    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:30:00", "2019-06-13 9:40:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
      object = JSON.parse(@response.body)
      # assert_nil object
      uuid = object["data"]["createReservation"]["id"]

    post "/graphql",
      params: {
      	"query": updateReservationQuery("2019-06-13 9:05:00", "2019-06-13 9:25:00", uuid)
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["errors"][0]["message"], "Invalid input: the time slot requested is not available on this item"
  end

  test "delete old reservation and create new one" do
    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:10:00", "2019-06-13 9:20:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    object = JSON.parse(@response.body)
    uuid = object["data"]["createReservation"]["id"]

    post "/graphql",
      params: {
      	"query": deleteReservationQuery(uuid)
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }

    post "/graphql",
      params: {
      	"query": createReservationQuery("2019-06-13 9:05:00", "2019-06-13 9:25:00")
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "2019-06-13T13:05:00Z", object["data"]["createReservation"]["startTime"]
    assert_equal "2019-06-13T13:25:00Z", object["data"]["createReservation"]["endTime"]
  end

  private
    def createReservationQuery(start_time, end_time, itemUuid="2b7652c1-40b0-4fca-843d-24ddd2a1ab01")
      tz = Item.find_by(uuid: itemUuid).time_zone
      "mutation {
        createReservation(
          itemId: \"#{itemUuid}\",
          startTime: \"#{Time.find_zone(tz).parse(start_time).utc.iso8601}\",
          endTime: \"#{Time.find_zone(tz).parse(end_time).utc.iso8601}\"
        ) {
            id
            startTime
            endTime
            item {
              id
            }
          }
      }"
    end

    def updateReservationQuery(start_time, end_time, uuid)
      tz = Reservation.find_by(uuid: uuid).item.time_zone
      "mutation {
        updateReservation(
          id: \"#{uuid}\",
          startTime: \"#{Time.find_zone(tz).parse(start_time).utc.iso8601}\",
          endTime: \"#{Time.find_zone(tz).parse(end_time).utc.iso8601}\"
        ) {
            id
            startTime
            endTime
            item {
              id
            }
          }
      }"
    end

    def deleteReservationQuery(uuid)
      "mutation {
        deleteReservation(
          id:\"#{uuid}\"
        ) {
          success
        }
      }"
    end


end
