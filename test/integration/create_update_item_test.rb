require 'test_helper'

class CreateUpdateItemTest < ActionDispatch::IntegrationTest
  test "create item new group" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    createItem(
                      groupName: \"Group 3\",
                      name: \"New Item\"
                    ) {
                      name
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["data"]["createItem"]["name"], "New Item"
    @newGroup = Group.find_by_name("Group 3")
    @item = Item.find_by_name("New Item")
    assert_equal @item.group, @newGroup
    assert_equal @newGroup.name, "Group 3"
    assert_equal @newGroup.items.last, @item
  end

  test "create item old group" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    createItem(
                      groupId: \"2c236840-2294-4846-9d95-e55526a92aee\",
                      name: \"New Item\"
                    ) {
                      name
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["data"]["createItem"]["name"], "New Item"
    @newGroup = Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee")
    @item = Item.find_by_name("New Item")
    assert_equal @item.group, @newGroup
    assert_equal @newGroup.name, "First Group"
    assert_equal @newGroup.items.last, @item
  end

  test "update item" do
    open_time = Time.now.to_i
    close_time = (Time.now + 20.minutes).to_i
    post "/graphql",
      params: {
      	"query": "mutation {
                    updateItem(
                      id: \"7a5c4dad-5851-4df7-bcc5-a4198f1f3eda\",
                      name: \"Updated Item\",
                      imgPath: \"new_img_path\",
                      openTime: #{open_time},
                      closeTime: #{close_time},
                      timeZone: \"Central Time (US & Canada)\",
                      maxReservationLength: 15,
                      description: \"some description\"
                    ) {
                      name
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["data"]["updateItem"]["name"], "Updated Item"
    @item = Item.find_by(uuid: "7a5c4dad-5851-4df7-bcc5-a4198f1f3eda")
    assert_equal @item.name, "Updated Item"
    assert_equal @item.img_path, "new_img_path"
    assert_equal @item.open_time, open_time
    assert_equal @item.close_time, close_time
    assert_equal @item.max_reservation_length, 15
    assert_equal @item.description, "some description"
  end
end
