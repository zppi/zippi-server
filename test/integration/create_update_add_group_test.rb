require 'test_helper'

class CreateUpdateJoinGroupTest < ActionDispatch::IntegrationTest

  test "create new group no session token" do
    oldGroupCount = Group.count
    post "/graphql",
      params: {
      	"query": "mutation {
                    createGroup(
                      name: \"Group 3\"
                    ) {
                      name
                      id
                    }
                  }"
      }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "You must be logged in to create a group", object["errors"][0]["message"]
    assert_nil Group.find_by_name('Group 3')
    assert_equal oldGroupCount, Group.count
  end

  test "create new group bad session token" do
    oldGroupCount = Group.count
    post "/graphql",
      params: {
      	"query": "mutation {
                    createGroup(
                      name: \"Group 3\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer bad_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "You must be logged in to create a group", object["errors"][0]["message"]
    assert_nil Group.find_by_name('Group 3')
    assert_equal oldGroupCount, Group.count
  end

  test "create new group no name" do
    oldGroupCount = Group.count
    post "/graphql",
      params: {
      	"query": "mutation {
                    createGroup(
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Field 'createGroup' is missing required arguments: name", object["errors"][0]["message"]
    assert_equal oldGroupCount, Group.count
  end

  test "create new group empty name" do
    oldGroupCount = Group.count
    post "/graphql",
      params: {
      	"query": "mutation {
                    createGroup(
                      name: ""
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Argument 'name' on Field 'createGroup' has an invalid value. Expected type 'String!'.", object["errors"][0]["message"]
    assert_equal oldGroupCount, Group.count
  end

  test "create new group with taken name" do
    oldGroupCount = Group.count
    post "/graphql",
      params: {
      	"query": "mutation {
                    createGroup(
                      name: \"First Group\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Invalid input: group name First Group is already taken", object["errors"][0]["message"]
    assert_equal oldGroupCount, Group.count
  end

  test "create new group" do
    oldGroupCount = Group.count
    post "/graphql",
      params: {
      	"query": "mutation {
                    createGroup(
                      name: \"Third Group\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["data"]["createGroup"]["name"], "Third Group"
    assert_equal !!object["data"]["createGroup"]["id"], true
    assert_equal oldGroupCount+1, Group.count
  end

  test "update group with bad session token" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    updateGroup(
                      name: \"Third Group\",
                      id: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer unverified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "You must be logged in to update a group", object["errors"][0]["message"]
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").name, "First Group"
  end

  test "update group with bad uuid" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    updateGroup(
                      name: \"Third Group\",
                      id: \"bad_uuid\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Invalid input: the id you provided does not match a group in the database", object["errors"][0]["message"]
  end

  test "update group with empty name" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    updateGroup(
                      name: \"\",
                      id: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Invalid input: one or more values passed in to update the group was not acceptable", object["errors"][0]["message"]
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").name, "First Group"
  end

  test "update group with no name" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    updateGroup(
                      id: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["data"]["updateGroup"]["name"], "First Group"
    assert_equal !!object["data"]["updateGroup"]["id"], true
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").name, "First Group"
  end

  test "update group with taken name" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    updateGroup(
                      name: \"Second Group\",
                      id: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Invalid input: group name Second Group is already taken", object["errors"][0]["message"]
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").name, "First Group"
  end

  test "update group not member of" do
    post "/graphql",
      params: {
        "query": "mutation {
                    updateGroup(
                      name: \"Third Group\",
                      id: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer valid_token1' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "You must be a member of the group to update it", object["errors"][0]["message"]
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").name, "First Group"
  end

  test "update group" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    updateGroup(
                      name: \"Third Group\",
                      id: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      name
                      id
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer verified_session_token' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["data"]["updateGroup"]["name"], "Third Group"
    assert_equal !!object["data"]["updateGroup"]["id"], true
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").name, "Third Group"
  end

  test "join group" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    joinGroup(
                      groupId: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      groups {
                        id
                      }
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer valid_token1' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal object["data"]["joinGroup"]["groups"].include?({"id" => "2c236840-2294-4846-9d95-e55526a92aee"}), true
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").users.include?(User.find(1)), true
  end

  test "join group with bad session token" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    joinGroup(
                      groupId: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      groups {
                        id
                      }
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer invalid_token1' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "You must be logged in to add this group", object["errors"][0]["message"]
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").users.include?(User.find(1)), false
  end

  test "join group with bad group uuid" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    joinGroup(
                      groupId: \"bad_uuid\"
                    ) {
                      groups {
                        id
                      }
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer valid_token1' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Invalid input: the group id you provided does not match a group in the database", object["errors"][0]["message"]
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").users.include?(User.find(1)), false
  end

  test "join group they are already a part of" do
    post "/graphql",
      params: {
      	"query": "mutation {
                    joinGroup(
                      groupId: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      groups {
                        id
                      }
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer valid_token1' }
    post "/graphql",
      params: {
      	"query": "mutation {
                    joinGroup(
                      groupId: \"2c236840-2294-4846-9d95-e55526a92aee\"
                    ) {
                      groups {
                        id
                      }
                    }
                  }"
      },
      headers: { 'Authorization': 'Bearer valid_token1' }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "You are already a member of this group", object["errors"][0]["message"]
    assert_equal Group.find_by(uuid: "2c236840-2294-4846-9d95-e55526a92aee").users.include?(User.find(1)), true
  end


end
