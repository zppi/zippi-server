require 'test_helper'

class UsersLogoutTest < ActionDispatch::IntegrationTest

  test "logout user with invalid session_token" do
    assert_nil AuthToken.find_by_session_token('invalid_token')
    post "/graphql",
      params: {
      	"query": "mutation { deleteAuthToken(sessionToken: \"invalid_token\") { message success }}"
      }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Invalid input: invalid session_token", object["errors"][0]["message"]
    assert_nil AuthToken.find_by_session_token('invalid_token')
  end

  test "logout user with valid session_token" do
    assert_equal !!AuthToken.find_by_session_token('valid_token1'), true
    post "/graphql",
      params: {
      	"query": "mutation { deleteAuthToken(sessionToken: \"valid_token1\") { message success }}"
      }
    assert_response :success
    object = JSON.parse(@response.body)
    assert_equal "Delete authToken", object["data"]["deleteAuthToken"]["message"]
    assert_equal true, object["data"]["deleteAuthToken"]["success"]
    assert_nil AuthToken.find_by_session_token('valid_token1')
  end

end
