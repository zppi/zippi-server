FactoryBot.define do
  factory :user do
    name { Faker::Internet.email }
  end
end