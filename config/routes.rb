Rails.application.routes.draw do
  get 'qr/create'
  get "/graphql", to: "graphql#execute"
  post "/graphql", to: "graphql#execute"

  root 'application#hello'

  get '/app_redirect', to: 'verification#app_redirect'

  post '/item_image', to: 'item#image'

  get '/qr/sticker', to: 'qr#sticker'
end
